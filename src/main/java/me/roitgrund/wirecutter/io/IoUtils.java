package me.roitgrund.wirecutter.io;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

public class IoUtils {
    public static InputStream stream(String in) {
        return new ByteArrayInputStream(in.getBytes(StandardCharsets.UTF_8));
    }

    public static long getSize(FileChannel fileChannel) {
        try {
            return fileChannel.size();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static FileChannel getFileChannel(Path path) {
        try {
            return FileChannel.open(path, StandardOpenOption.READ);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static MappedByteBuffer map(FileChannel fileChannel, FileChannel.MapMode mode, int position, long size) {
        try {
            return fileChannel.map(mode, position, size);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
