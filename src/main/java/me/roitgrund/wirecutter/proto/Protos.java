package me.roitgrund.wirecutter.proto;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.DatagramPacket;
import java.util.Optional;

public class Protos {
    @FunctionalInterface
    public interface Parser<T> {
        T parseFrom(InputStream input) throws IOException;
    }

    public static <T> Optional<T> parse(DatagramPacket datagramPacket, Parser<T> parser) {
        try {
            return Optional.of(parser.parseFrom(new ByteArrayInputStream(
                    datagramPacket.getData(), datagramPacket.getOffset(), datagramPacket.getLength())));
        } catch (IOException e) {
            return Optional.empty();
        }
    }
}
