package me.roitgrund.wirecutter.crypto;

import com.google.common.io.BaseEncoding;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class HashUtils {
    private static final MessageDigest digest;

    @FunctionalInterface
    public interface Proto {
        byte[] toByteArray();
    }

    static {
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    private HashUtils() {}

    public static String hashProto(Proto input) {
        byte[] hash = digest.digest(input.toByteArray());
        return BaseEncoding.base64().encode(hash);
    }
}
