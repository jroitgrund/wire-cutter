package me.roitgrund.wirecutter.pairing;

import io.grpc.netty.shaded.io.grpc.netty.NettyServerBuilder;
import me.roitgrund.wirecutter.Wirecutter;
import me.roitgrund.wirecutter.pairing.broadcast.BroadcastInterface;
import me.roitgrund.wirecutter.pairing.broadcast.BroadcastListener;
import me.roitgrund.wirecutter.pairing.broadcast.Broadcaster;
import me.roitgrund.wirecutter.pairing.broadcast.InterfaceInfo;
import me.roitgrund.wirecutter.pairing.x509.CertAndKey;
import me.roitgrund.wirecutter.pairing.x509.CertGen;

import java.util.function.Consumer;

public class PairingFactory {
    public static Pairing create(
            String name,
            String id,
            String interfaceName,
            Consumer<Wirecutter.Fingerprint> onPairingRequest,
            Consumer<NettyServerBuilder> buildServer) {
        InterfaceInfo interfaceInfo = BroadcastInterface.byName(interfaceName).info();
        CertAndKey ownCertAndKey = CertGen.genCert(interfaceInfo.getOwnAddress());
        Wirecutter.PairingRequest ownPairingRequest = getPairingRequest(name, id, ownCertAndKey, interfaceInfo);

        BroadcastListener broadcastListener = new BroadcastListener();
        Broadcaster broadcaster = new Broadcaster(ownPairingRequest.toByteArray(), interfaceInfo.getBroadcastAddress());

        return new Pairing(
                ownPairingRequest, ownCertAndKey, onPairingRequest, buildServer, broadcaster, broadcastListener);
    }

    private static Wirecutter.PairingRequest getPairingRequest(
            String name, String id, CertAndKey ownCertAndKey, InterfaceInfo interfaceInfo) {
        return Wirecutter.PairingRequest.newBuilder()
                .setCert(ownCertAndKey.getCert())
                .setName(name)
                .setId(id)
                .setIp(interfaceInfo.getOwnAddress().getHostAddress())
                .build();
    }
}
