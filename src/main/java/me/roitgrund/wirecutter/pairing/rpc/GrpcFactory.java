package me.roitgrund.wirecutter.pairing.rpc;

import io.grpc.Server;
import io.grpc.netty.shaded.io.grpc.netty.GrpcSslContexts;
import io.grpc.netty.shaded.io.grpc.netty.NettyServerBuilder;
import io.grpc.netty.shaded.io.netty.handler.ssl.ClientAuth;
import io.grpc.stub.StreamObserver;
import me.roitgrund.wirecutter.WireCutterGrpc;
import me.roitgrund.wirecutter.Wirecutter;
import me.roitgrund.wirecutter.pairing.x509.CertAndKey;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.function.Consumer;

import static me.roitgrund.wirecutter.pairing.rpc.RpcProtocol.RPC_PORT;

public class GrpcFactory {
    private GrpcFactory() {}

    public static Server start(
            CertAndKey ownCertAndKey,
            String clientCert,
            Runnable onClientConfirmPairing,
            Consumer<NettyServerBuilder> buildServer) throws IOException {
        NettyServerBuilder builder = NettyServerBuilder.forPort(RPC_PORT);
        buildServer.accept(builder);
        Server rpcServer = builder
                .sslContext(GrpcSslContexts.forServer(stream(ownCertAndKey.getCert()), stream(ownCertAndKey.getKey()))
                        .trustManager(stream(clientCert))
                        .clientAuth(ClientAuth.REQUIRE)
                        .build())
                .addService(new WireCutterGrpc.WireCutterImplBase() {
                    @Override
                    public void confirmPairing(
                            Wirecutter.Empty request, StreamObserver<Wirecutter.Empty> responseObserver) {
                        responseObserver.onNext(Wirecutter.Empty.getDefaultInstance());
                        responseObserver.onCompleted();
                        onClientConfirmPairing.run();
                    }
                })
                .build();
        rpcServer.start();

        return rpcServer;
    }

    private static ByteArrayInputStream stream(String key) {
        return new ByteArrayInputStream(key.getBytes(StandardCharsets.UTF_8));
    }
}
