package me.roitgrund.wirecutter.pairing;

import io.grpc.ManagedChannel;
import io.grpc.Server;

public class PairingResult {
    private final Server server;
    private final ManagedChannel clientChannel;

    public PairingResult(Server server, ManagedChannel clientChannel) {
        this.server = server;
        this.clientChannel = clientChannel;
    }

    public Server getServer() {
        return server;
    }

    public ManagedChannel getClientChannel() {
        return clientChannel;
    }
}
