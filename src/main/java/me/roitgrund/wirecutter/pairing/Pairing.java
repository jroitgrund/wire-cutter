package me.roitgrund.wirecutter.pairing;

import io.grpc.ManagedChannel;
import io.grpc.Server;
import io.grpc.netty.shaded.io.grpc.netty.GrpcSslContexts;
import io.grpc.netty.shaded.io.grpc.netty.NettyChannelBuilder;
import io.grpc.netty.shaded.io.grpc.netty.NettyServerBuilder;
import me.roitgrund.wirecutter.WireCutterGrpc;
import me.roitgrund.wirecutter.Wirecutter;
import me.roitgrund.wirecutter.crypto.HashUtils;
import me.roitgrund.wirecutter.io.IoUtils;
import me.roitgrund.wirecutter.pairing.broadcast.BroadcastListener;
import me.roitgrund.wirecutter.pairing.broadcast.Broadcaster;
import me.roitgrund.wirecutter.pairing.rpc.GrpcFactory;
import me.roitgrund.wirecutter.pairing.x509.CertAndKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.Consumer;

import static com.google.common.base.Preconditions.checkArgument;
import static me.roitgrund.wirecutter.pairing.rpc.RpcProtocol.RPC_PORT;

public class Pairing {
    private static final Logger log = LoggerFactory.getLogger(Pairing.class);

    private final Wirecutter.PairingRequest ownPairingRequest;
    private final CertAndKey ownCertAndKey;
    private final Consumer<Wirecutter.Fingerprint> onPairingRequest;
    private final Consumer<NettyServerBuilder> buildServer;
    private final Broadcaster broadcaster;
    private final BroadcastListener broadcastListener;
    private final Set<Wirecutter.PairingRequest> pairingRequests;
    private final CountDownLatch clientConfirmation;

    public Pairing(
            Wirecutter.PairingRequest ownPairingRequest,
            CertAndKey ownCertAndKey,
            Consumer<Wirecutter.Fingerprint> onPairingRequest,
            Consumer<NettyServerBuilder> buildServer,
            Broadcaster broadcaster,
            BroadcastListener broadcastListener) {
        this.ownPairingRequest = ownPairingRequest;
        this.ownCertAndKey = ownCertAndKey;
        this.onPairingRequest = onPairingRequest;
        this.buildServer = buildServer;
        this.broadcaster = broadcaster;
        this.broadcastListener = broadcastListener;
        this.pairingRequests = new HashSet<>();
        clientConfirmation = new CountDownLatch(1);
    }

    public Wirecutter.Fingerprint start() {
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        executorService.submit(broadcaster::broadcast);
        executorService.submit(() -> broadcastListener.listen(this::onPairingRequest));

        return fingerprint(ownPairingRequest);
    }

    public Future<PairingResult> pairWithClient(Wirecutter.Fingerprint fingerprint) {
        broadcastListener.stop();
        return Executors.newSingleThreadExecutor().submit(() -> {
            checkArgument(pairingRequests.contains(fingerprint.getRequest()));

            ManagedChannel clientChannel;
            Server server;

            try {
                server = GrpcFactory.start(
                        ownCertAndKey,
                        fingerprint.getRequest().getCert(),
                        this::onClientConfirmPairing,
                        buildServer);
                clientChannel = getChannel(fingerprint.getRequest());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            sayHello(getClient(clientChannel));
            try {
                clientConfirmation.await();
            } catch (InterruptedException e) {
                log.error("Interrupted while waiting for client confirmation.", e);
                Thread.currentThread().interrupt();
            }

            return new PairingResult(server, clientChannel);
        });
    }

    private void onClientConfirmPairing() {
        broadcaster.stop();
        clientConfirmation.countDown();
    }

    private void sayHello(WireCutterGrpc.WireCutterBlockingStub wireCutterBlockingStub) {
        Optional<Wirecutter.Empty> ack = Optional.empty();
        while (!ack.isPresent()) {
            try {
                ack = Optional.of(wireCutterBlockingStub.confirmPairing(Wirecutter.Empty.getDefaultInstance()));
            } catch (Throwable ignored) {
                log.debug("Couldn't reach pair yet.", ignored);
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                log.error("Interrupted while polling pair.", e);
                Thread.currentThread().interrupt();
                return;
            }
        }
    }

    private void onPairingRequest(Wirecutter.PairingRequest pairingRequest) {
        if (!pairingRequest.equals(ownPairingRequest)) {
            pairingRequests.add(pairingRequest);
            onPairingRequest.accept(fingerprint(pairingRequest));
        }
    }

    private static Wirecutter.Fingerprint fingerprint(Wirecutter.PairingRequest pairingRequest) {
        return Wirecutter.Fingerprint.newBuilder()
                .setRequest(pairingRequest)
                .setFingerprint(HashUtils.hashProto(pairingRequest::toByteArray))
                .build();
    }

    private WireCutterGrpc.WireCutterBlockingStub getClient(ManagedChannel channel) {
        return WireCutterGrpc.newBlockingStub(channel);
    }

    private ManagedChannel getChannel(Wirecutter.PairingRequest pairingRequest) throws SSLException {
        return NettyChannelBuilder
                .forAddress(pairingRequest.getIp(), RPC_PORT)
                .sslContext(GrpcSslContexts
                        .forClient()
                        .trustManager(IoUtils.stream(pairingRequest.getCert()))
                        .keyManager(IoUtils.stream(ownCertAndKey.getCert()), IoUtils.stream(ownCertAndKey.getKey()))
                        .build())
                .build();
    }
}
