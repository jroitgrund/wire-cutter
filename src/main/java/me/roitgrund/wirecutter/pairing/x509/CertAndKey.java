package me.roitgrund.wirecutter.pairing.x509;

public class CertAndKey {
    private final String cert;
    private final String key;

    public CertAndKey(String cert, String key) {
        this.cert = cert;
        this.key = key;
    }

    public String getCert() {
        return cert;
    }

    public String getKey() {
        return key;
    }
}
