package me.roitgrund.wirecutter.pairing.x509;

import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.X500NameBuilder;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.asn1.x509.*;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.jcajce.JcaPEMWriter;
import org.bouncycastle.openssl.jcajce.JcaPKCS8Generator;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.util.io.pem.PemObject;

import java.io.StringWriter;
import java.math.BigInteger;
import java.net.InetAddress;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.time.LocalDate;
import java.util.Date;

import static java.time.ZoneOffset.UTC;

public class CertGen {
    private CertGen() {}

    public static CertAndKey genCert(InetAddress inetAddress) {
        try {
            SecureRandom random = new SecureRandom();

            KeyPairGenerator keypairGen = KeyPairGenerator.getInstance("EC");
            keypairGen.initialize(256, random);
            KeyPair keypair = keypairGen.generateKeyPair();

            X500Name subject = new X500NameBuilder(BCStyle.INSTANCE)
                    .addRDN(BCStyle.CN, inetAddress.getHostAddress())
                    .build();

            byte[] id = new byte[20];
            random.nextBytes(id);
            BigInteger serial = new BigInteger(160, random);

            X509v3CertificateBuilder certificate = new JcaX509v3CertificateBuilder(
                    subject,
                    serial,
                    Date.from(LocalDate.now(UTC).atStartOfDay().toInstant(UTC)),
                    Date.from(LocalDate.now(UTC).plusYears(50).atStartOfDay().toInstant(UTC)),
                    subject,
                    keypair.getPublic());

            certificate.addExtension(Extension.subjectKeyIdentifier, false, new SubjectKeyIdentifier(id).getEncoded());
            certificate.addExtension(Extension.authorityKeyIdentifier, false, new AuthorityKeyIdentifier(id).getEncoded());
            certificate.addExtension(
                    Extension.basicConstraints,
                    true,
                    new BasicConstraints(true).getEncoded());
            certificate.addExtension(
                    Extension.keyUsage,
                    false,
                    new KeyUsage(KeyUsage.keyCertSign | KeyUsage.digitalSignature).getEncoded());
            certificate.addExtension(
                    Extension.extendedKeyUsage,
                    false,
                    new ExtendedKeyUsage(new KeyPurposeId[]{
                            KeyPurposeId.id_kp_serverAuth,
                            KeyPurposeId.id_kp_clientAuth
                    }).getEncoded());
            certificate.addExtension(
                    Extension.subjectAlternativeName,
                    false,
                    new GeneralNames(new GeneralName(GeneralName.iPAddress, inetAddress.getHostAddress())));

            ContentSigner signer = new JcaContentSignerBuilder("SHA256withECDSA")
                    .build(keypair.getPrivate());
            X509CertificateHolder holder = certificate.build(signer);

            JcaX509CertificateConverter converter = new JcaX509CertificateConverter();
            converter.setProvider(new BouncyCastleProvider());
            X509Certificate x509 = converter.getCertificate(holder);

            JcaPKCS8Generator generator = new JcaPKCS8Generator(keypair.getPrivate(), null);
            PemObject privateKey = generator.generate();

            StringWriter cert = new StringWriter();
            StringWriter key = new StringWriter();

            try (JcaPEMWriter certWriter = new JcaPEMWriter(cert)) {
                certWriter.writeObject(x509);
                certWriter.flush();
            }

            try (JcaPEMWriter keyWriter = new JcaPEMWriter(key)) {
                keyWriter.writeObject(privateKey);
                keyWriter.flush();
            }

            return new CertAndKey(cert.toString(), key.toString());
        } catch (Throwable t) {
            throw new RuntimeException(t);
        }
    }
}
