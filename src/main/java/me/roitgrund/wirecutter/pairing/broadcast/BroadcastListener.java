package me.roitgrund.wirecutter.pairing.broadcast;

import me.roitgrund.wirecutter.Wirecutter;
import me.roitgrund.wirecutter.proto.Protos;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.function.Consumer;

import static me.roitgrund.wirecutter.pairing.broadcast.BroadcastProtocol.BROADCAST_PORT;

public class BroadcastListener {
    private final byte[] buf;
    private volatile boolean listen;

    public BroadcastListener() {
        buf = new byte[1048576];
        listen = true;
    }

    public void stop() {
        listen = false;
    }

    public void listen(Consumer<Wirecutter.PairingRequest> onPairingRequest) {
        try {
            DatagramSocket datagramSocket = new DatagramSocket(BROADCAST_PORT);

            while (listen) {
                DatagramPacket packet = new DatagramPacket(buf, buf.length);
                datagramSocket.receive(packet);

                Protos.parse(packet, Wirecutter.PairingRequest::parseFrom).ifPresent(onPairingRequest);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
