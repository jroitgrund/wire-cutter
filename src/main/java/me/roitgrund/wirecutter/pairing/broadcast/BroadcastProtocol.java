package me.roitgrund.wirecutter.pairing.broadcast;

public class BroadcastProtocol {
    private BroadcastProtocol() {}

    public static final int BROADCAST_PORT = 29875;
}
