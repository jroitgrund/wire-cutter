package me.roitgrund.wirecutter.pairing.broadcast;

import java.net.InetAddress;

public class InterfaceInfo {
    private final InetAddress broadcastAddress;
    private final InetAddress ownAddress;

    public InterfaceInfo(InetAddress broadcastAddress, InetAddress ownAddress) {
        this.broadcastAddress = broadcastAddress;
        this.ownAddress = ownAddress;
    }

    public InetAddress getBroadcastAddress() {
        return broadcastAddress;
    }

    public InetAddress getOwnAddress() {
        return ownAddress;
    }
}
