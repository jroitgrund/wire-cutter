package me.roitgrund.wirecutter.pairing.broadcast;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import static me.roitgrund.wirecutter.pairing.broadcast.BroadcastProtocol.BROADCAST_PORT;

public class Broadcaster {
    private static final Logger log = LoggerFactory.getLogger(Broadcaster.class);

    private final byte[] message;
    private final InetAddress broadcastAddress;
    private volatile boolean continueBroadcasting;

    public Broadcaster(byte[] message, InetAddress broadcastAddress) {
        this.broadcastAddress = broadcastAddress;
        this.message = message;
        continueBroadcasting = true;
    }

    public void stop() {
        continueBroadcasting = false;
    }

    public void broadcast() {
        try {
            DatagramSocket datagramSocket = new DatagramSocket();
            while (continueBroadcasting) {
                DatagramPacket datagramPacket =
                        new DatagramPacket(message, message.length, broadcastAddress, BROADCAST_PORT);
                datagramSocket.send(datagramPacket);

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    log.error("Interrupted while broadcasting", e);
                    Thread.currentThread().interrupt();
                    return;
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
