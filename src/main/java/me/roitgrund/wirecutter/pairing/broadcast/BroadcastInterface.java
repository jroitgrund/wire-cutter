package me.roitgrund.wirecutter.pairing.broadcast;

import java.net.*;
import java.util.Collections;

import static com.google.common.base.Preconditions.checkNotNull;

public class BroadcastInterface {
    private final NetworkInterface networkInterface;

    private BroadcastInterface(NetworkInterface networkInterface) {
        this.networkInterface = networkInterface;
    }

    public static BroadcastInterface byName(String name) {
        NetworkInterface networkInterface;
        try {
            networkInterface = Collections.list(NetworkInterface.getNetworkInterfaces()).stream()
                    .filter(iface -> iface.getName().equals(name))
                    .findAny()
                    .orElseThrow(RuntimeException::new);
        } catch (SocketException e) {
            throw new RuntimeException(e);
        }
        return new BroadcastInterface(networkInterface);
    }

    public InterfaceInfo info() {
        InterfaceAddress ipv4InterfaceAddresses = networkInterface.getInterfaceAddresses()
                .stream()
                .filter(interfaceAddress -> interfaceAddress.getAddress() instanceof Inet4Address)
                .findAny()
                .orElseThrow(RuntimeException::new);

        InetAddress broadcastAddress = checkNotNull(ipv4InterfaceAddresses.getBroadcast());
        InetAddress ownAddress = checkNotNull(ipv4InterfaceAddresses.getAddress());

        return new InterfaceInfo(broadcastAddress, ownAddress);
    }
}
