package me.roitgrund.wirecutter.cli;

import me.roitgrund.wirecutter.FileTransferGrpc;
import me.roitgrund.wirecutter.pairing.PairingResult;
import me.roitgrund.wirecutter.transfer.FileTransferClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Paths;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;

public class SendingFileState implements CLIState {
    private static final Logger log = LoggerFactory.getLogger(SendingFileState.class);
    private final CLIStateUtils cli;
    private final PairingResult pairingResult;
    private final CountDownLatch receivedFile;

    public SendingFileState(CLIStateUtils cli, PairingResult pairingResult, CountDownLatch receivedFile) {
        this.cli = cli;
        this.pairingResult = pairingResult;
        this.receivedFile = receivedFile;
    }

    @Override
    public void processCommand(String command) {

    }

    @Override
    public void init() {
        FileTransferGrpc.FileTransferStub fileTransferStub =
                FileTransferGrpc.newStub(pairingResult.getClientChannel());

        try {
            FileTransferClient.sendFile(Paths.get("random.txt"), fileTransferStub).get();
        } catch (InterruptedException e) {
            log.error("Interrupted while sending file", e);
            Thread.currentThread().interrupt();
            return;
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }

        try {
            receivedFile.await();
        } catch (InterruptedException e) {
            log.error("Interrupted while waiting to receive file", e);
            Thread.currentThread().interrupt();
            return;
        }

        cli.onDone();
    }
}
