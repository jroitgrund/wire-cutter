package me.roitgrund.wirecutter.cli;

import java.io.PrintWriter;

public interface CLIStateUtils {
    void setState(CLIState cliState);
    void onDone();
    PrintWriter writer();
}
