package me.roitgrund.wirecutter.cli;

import java.util.UUID;

public class InitState implements CLIState {
    private final CLIStateUtils cli;

    private int step = 0;
    private String name;
    private String id;

    public InitState(CLIStateUtils cli) {
        this.cli = cli;
    }

    @Override
    public void processCommand(String command) {
        switch (step++) {
            case 0:
                name = command;
                id = UUID.randomUUID().toString();
                cli.writer().println(String.format("Thanks %s! Now please enter an interface name.", name));
                break;
            case 1:
                cli.setState(new WaitForPairingRequestState(cli, name, id, command));
                break;
            default:
                break;
        }
    }

    @Override
    public void init() {
        cli.writer().println("Welcome to wire-cutter. Please enter a name.");
    }
}
