package me.roitgrund.wirecutter.cli;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class CLI implements CLIStateUtils {

    private final PrintWriter writer;
    private CLIState state;

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        PrintWriter writer = new PrintWriter(System.out, true);
        CLI cli = new CLI(writer);
        cli.init();
        String line;
        while ((line = reader.readLine()) != null) {
            cli.processCommand(line);
        }
    }

    private CLI(PrintWriter writer) {
        this.writer = writer;
        this.state = new InitState(this);
    }

    public PrintWriter writer() {
        return writer;
    }

    public void onDone() {
        writer.println("Done!");
        System.exit(0);
    }

    public void setState(CLIState state) {
        this.state = state;
        state.init();
    }

    private void init() {
        state.init();
    }

    private void processCommand(String command) {
        state.processCommand(command);
    }
}
