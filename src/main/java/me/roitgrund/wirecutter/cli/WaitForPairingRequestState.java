package me.roitgrund.wirecutter.cli;

import io.grpc.netty.shaded.io.grpc.netty.NettyServerBuilder;
import me.roitgrund.wirecutter.Wirecutter;
import me.roitgrund.wirecutter.pairing.Pairing;
import me.roitgrund.wirecutter.pairing.PairingFactory;
import me.roitgrund.wirecutter.transfer.FileTransfer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Paths;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;

public class WaitForPairingRequestState implements CLIState {
    private static final Logger log = LoggerFactory.getLogger(WaitForPairingRequestState.class);

    private final CLIStateUtils cli;
    private final String name;
    private final String id;
    private final String interfaceName;
    private final BlockingQueue<String> inputQueue;
    private Pairing pairing;
    private CountDownLatch receivedFile;

    public WaitForPairingRequestState(
            CLIStateUtils cli, String name, String id, String interfaceName) {
        this.cli = cli;
        this.name = name;
        this.id = id;
        this.interfaceName = interfaceName;
        inputQueue = new ArrayBlockingQueue<>(1);
        receivedFile = new CountDownLatch(1);
    }

    @Override
    public void processCommand(String command) {
        inputQueue.offer(command);
    }

    @Override
    public void init() {
        pairing = PairingFactory.create(
                name,
                id,
                interfaceName,
                this::onPairingRequest,
                nettyServerBuilder -> {
                    buildServer(nettyServerBuilder, receivedFile);
                });
        Wirecutter.Fingerprint ownFingerprint = pairing.start();
        cli.writer().println(
                String.format("Your unique fingerprint is %s.", ownFingerprint.getFingerprint()));
    }

    private void onPairingRequest(Wirecutter.Fingerprint pairingRequest) {
        cli.writer().println(String.format(
                "Connection request from %s.\nDo you recognize this fingerprint (Y/N)?\n%s",
                pairingRequest.getRequest().getName(),
                pairingRequest.getFingerprint()));

        if (getInput().trim().toLowerCase().equals("y")) {
            try {
                cli.writer().println(String.format("OK! Pairing with %s", pairingRequest.getRequest().getName()));
                cli.setState(new SendingFileState(cli, pairing.pairWithClient(pairingRequest).get(), receivedFile));
            } catch (InterruptedException e) {
                log.error("Interrupted while pairing with client.", e);
                Thread.currentThread().interrupt();
                return;
            } catch (ExecutionException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private String getInput() {
        try {
            return inputQueue.take();
        } catch (InterruptedException e) {
            log.error("Interrupted while waiting for input.", e);
            Thread.currentThread().interrupt();
            return null;
        }
    }

    private void buildServer(NettyServerBuilder nettyServerBuilder, CountDownLatch receivedFile) {
        nettyServerBuilder.addService(new FileTransfer(Paths.get("received"), $ -> receivedFile.countDown()));
    }
}
