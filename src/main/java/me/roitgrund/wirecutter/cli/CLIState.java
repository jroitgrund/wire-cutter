package me.roitgrund.wirecutter.cli;

public interface CLIState {
    void processCommand(String command);
    void init();
}
