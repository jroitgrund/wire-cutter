package me.roitgrund.wirecutter.transfer;

import com.google.protobuf.ByteString;
import io.grpc.stub.StreamObserver;
import me.roitgrund.wirecutter.FileTransferGrpc;
import me.roitgrund.wirecutter.Wirecutter;
import me.roitgrund.wirecutter.io.IoUtils;

import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.util.concurrent.CompletableFuture;

import static com.google.common.base.Preconditions.checkNotNull;

public class FileTransferClient {
    public static CompletableFuture<Wirecutter.Empty> sendFile(
            Path path, FileTransferGrpc.FileTransferStub fileTransfer) {
        FileChannel fileChannel = IoUtils.getFileChannel(path);
        long size = IoUtils.getSize(fileChannel);

        CompletableFuture<Wirecutter.Empty> result = new CompletableFuture<>();
        Wirecutter.Metadata metadata = Wirecutter.Metadata.newBuilder()
                    .setFilename(path.getFileName().toString())
                    .setSizeBytes(size)
                    .build();
        MappedByteBuffer buffer = IoUtils.map(fileChannel, FileChannel.MapMode.READ_ONLY, 0, size);

        StreamObserver<Wirecutter.SendFileRequest> request =
                    fileTransfer.sendFile(new StreamObserver<Wirecutter.Empty>() {
                        private Wirecutter.Empty value;

                        @Override
                        public void onNext(Wirecutter.Empty value) {
                            this.value = value;
                        }

                        @Override
                        public void onError(Throwable t) {

                        }

                        @Override
                        public void onCompleted() {
                            try {
                                fileChannel.close();
                            } catch (IOException e) {
                                throw new RuntimeException(e);
                            }
                            result.complete(checkNotNull(value));
                        }
                    });

        request.onNext(Wirecutter.SendFileRequest.newBuilder()
                .setMetadata(metadata)
                .build());

        while (buffer.hasRemaining()) {
            request.onNext(Wirecutter.SendFileRequest.newBuilder()
                    .setFile(ByteString.copyFrom(buffer, Math.min(65536, buffer.remaining())))
                    .build());
        }

        request.onCompleted();

        return result;
    }
}
