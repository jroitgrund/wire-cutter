package me.roitgrund.wirecutter.transfer;

import io.grpc.stub.StreamObserver;
import me.roitgrund.wirecutter.FileTransferGrpc;
import me.roitgrund.wirecutter.Wirecutter;

import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.function.Consumer;

import static com.google.common.base.Preconditions.checkState;

public class FileTransfer extends FileTransferGrpc.FileTransferImplBase {
    private final Path dest;
    private final Consumer<Wirecutter.Metadata> onDoneReceivingFile;

    public FileTransfer(Path dest) {
        this(dest, $ -> {});
    }

    public FileTransfer(Path dest, Consumer<Wirecutter.Metadata> onDoneReceivingFile) {
        this.dest = dest;
        this.onDoneReceivingFile = onDoneReceivingFile;
        checkState(Files.isDirectory(dest));
    }

    @Override
    public StreamObserver<Wirecutter.SendFileRequest> sendFile(StreamObserver<Wirecutter.Empty> responseObserver) {
        return new StreamObserver<Wirecutter.SendFileRequest>() {
            private Wirecutter.Metadata metadata;
            private MappedByteBuffer buffer;
            private FileChannel channel;

            @Override
            public void onNext(Wirecutter.SendFileRequest value) {
                switch (value.getMetadataOrBytesCase()) {
                    case METADATA:
                        try {
                            metadata = value.getMetadata();
                            channel = FileChannel.open(
                                    dest.resolve(metadata.getFilename()),
                                    StandardOpenOption.CREATE_NEW,
                                    StandardOpenOption.READ,
                                    StandardOpenOption.WRITE);
                            buffer = channel.map(FileChannel.MapMode.READ_WRITE, 0, value.getMetadata().getSizeBytes());
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                        break;
                    case FILE:
                        buffer.put(value.getFile().asReadOnlyByteBuffer());
                        break;
                    default:
                        throw new RuntimeException();
                }
            }

            @Override
            public void onError(Throwable t) {
                throw new RuntimeException(t);
            }

            @Override
            public void onCompleted() {
                try {
                    channel.close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                responseObserver.onNext(Wirecutter.Empty.getDefaultInstance());
                responseObserver.onCompleted();
                onDoneReceivingFile.accept(metadata);
            }
        };
    }
}
