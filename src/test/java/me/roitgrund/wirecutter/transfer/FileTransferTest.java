package me.roitgrund.wirecutter.transfer;

import com.google.common.io.BaseEncoding;
import com.google.common.io.Files;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import me.roitgrund.wirecutter.FileTransferGrpc;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Path;
import java.util.Random;
import java.util.concurrent.ExecutionException;

import static org.junit.Assert.assertArrayEquals;

public class FileTransferTest {
    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Test
    public void testFileTransfer() throws IOException, ExecutionException, InterruptedException {
        byte[] bytes = new byte[8388608];
        new Random().nextBytes(bytes);
        OutputStream outputStream = BaseEncoding.base64().encodingStream(new FileWriter(folder.newFile("random.txt")));
        outputStream.write(bytes);

        File dest = folder.newFolder();
        Server server = ServerBuilder.forPort(9876)
                .addService(new FileTransfer(dest.toPath()))
                .build();

        server.start();

        FileTransferGrpc.FileTransferStub client =
                FileTransferGrpc.newStub(ManagedChannelBuilder
                        .forAddress("localhost", 9876)
                        .usePlaintext()
                        .build());

        Path random = folder.getRoot().toPath().resolve("random.txt");
        Path randomReceived = dest.toPath().resolve("random.txt");
        FileTransferClient.sendFile(random, client).get();

        assertArrayEquals(
                Files.toByteArray(random.toFile()),
                Files.toByteArray(randomReceived.toFile()));
    }
}