package me.roitgrund.wirecutter.integration;

import com.palantir.docker.compose.DockerComposeRule;
import com.palantir.docker.compose.logging.LogDirectory;
import org.joda.time.Seconds;
import org.junit.ClassRule;
import org.junit.Test;

public class CliIntegrationTest {
    @ClassRule
    public static DockerComposeRule docker = DockerComposeRule.builder()
            .file("./src/test/resources/me/roitgrund/wirecutter/integration/docker-compose.yml")
            .saveLogsTo(LogDirectory.gradleDockerLogsDirectory(CliIntegrationTest.class))
            .nativeServiceHealthCheckTimeout(Seconds.seconds(60).toStandardDuration())
            .build();

    @Test
    public void test() {

    }
}
